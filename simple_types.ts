let firstName: String = "Dylan";
// let firstName = "Dylan";
// let firstName: string = "Dylan"; // type string
// firstName = 33; // attempts to re-assign the value to a different type
console.log(firstName);
console.log(typeof firstName);
