// let ourTuple: [number, boolean, string]
// ourTuple = [5, false, "Cooing God was here"]
// console.log(ourTuple)

// let ourTuple: [number, boolean, string]
// ourTuple = [5, false, "Coding God was here"]
// ourTuple.push("Something new and wrong")
// console.log(ourTuple)

const ourReadonlyTuple: readonly [number, boolean, string] = [5, true, 'The Real Coding God'];
// ourReadonlyTuple.push('Coding God took a day off');